import React, { Component } from "react";
import { compose } from "recompose";
import HeatmapLayer from "react-google-maps/lib/components/visualization/HeatmapLayer";
// import data from "./data.json";

const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} = require("react-google-maps");

const Heatmap = compose(
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultZoom={14}
    defaultCenter={{ lat: 37.783316, lng: -122.440023 }}
  >
    <HeatmapLayer
      options={{radius:50}}
      data={props.data.map(d => {
        return {
          location: new google.maps.LatLng(d.lat, d.lng),
          // weight:100
          // weight: d.weight
        };
      })}
    />
  </GoogleMap>
));

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentDidMount() {
    fetch("http://localhost:3030/itech/heatmap")
      .then(response => response.json())
      .then(data => this.setState({ data: data }));
  }

  render() {
    return (
      <div>
        <Heatmap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuyCq2grPNwGAcTLy6tivvobZdHXlE1V0&v=3.exp&libraries=visualization,geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `900px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          data={this.state.data}
        />
      </div>
    );
  }
}
