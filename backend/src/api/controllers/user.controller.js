const httpStatus = require("http-status");
const { omit } = require("lodash");
const _ = require("lodash");
const { handler: errorHandler } = require("../middlewares/error");
const User = require("../models/user.model");
const Location = require("../models/location");
const randomLocation = require("random-location");
const faker = require("faker");
const Geohash = require("latlon-geohash");
// Twitter HQ
const P = {
  latitude: 37.7768006,
  longitude: -122.4187928,
};

const R = 2*1000; // meters

let user;
(async () => {
  console.log("Creating new user==============");
  const userData = {
    email: "rpavez@gmail.com",
    password: "131231313323"
  };
  User.findOneAndUpdate(
    { email: userData.email },
    userData,
    { upsert: true },
    (err, doc) => {
      console.log('"Finished!');
      console.log("doc", doc);
      user = doc;
    }
  );
})();


// var geocoder = require('local-reverse-postalcode');
// console.log('Starting geocoder')
// geocoder.init(function() {
//   console.log('Geocoder started!')
  setInterval(async () => {
    if (user) {
      const coords = randomLocation.randomCirclePoint(P, R);
      const location = new Location({
        username: faker.internet.userName(),
        userId: user.get("id"),
        loc: { type: "Point", coordinates: [coords.longitude,coords.latitude] }
      });
      // Persist
      await location.save();
    }
  }, 1000);
// });

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const user = await User.get(id);
    req.locals = { user };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.user.transform());

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req, res) => res.json(req.user.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const user = new User(req.body);
    const savedUser = await user.save();
    res.status(httpStatus.CREATED);
    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Replace existing user
 * @public
 */
exports.replace = async (req, res, next) => {
  try {
    const { user } = req.locals;
    const newUser = new User(req.body);
    const ommitRole = user.role !== "admin" ? "role" : "";
    const newUserObject = omit(newUser.toObject(), "_id", ommitRole);

    await user.update(newUserObject, { override: true, upsert: true });
    const savedUser = await User.findById(user._id);

    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = (req, res, next) => {
  const ommitRole = req.locals.user.role !== "admin" ? "role" : "";
  const updatedUser = omit(req.body, ommitRole);
  const user = Object.assign(req.locals.user, updatedUser);

  user
    .save()
    .then(savedUser => res.json(savedUser.transform()))
    .catch(e => next(User.checkDuplicateEmail(e)));
};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const users = await User.list(req.query);
    const transformedUsers = users.map(user => user.transform());
    res.json(transformedUsers);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
exports.remove = (req, res, next) => {
  const { user } = req.locals;

  user
    .remove()
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};

/* eslint no-undef: 0 */
const getHeatMap = zoom => {
  if (!user) {
    console.log("no user:");
    return Promise.resolve(null);
  }

  return Location.aggregate([
    {
      $project: {
        username: 1,
        zipcode: 1,
        shortGeohash: { $substr: ["$geohash", 0, zoom] }
      }
    },
    {
      $group: {
        _id: "$shortGeohash",
        count: { $sum: 1 },
        originalDoc: { $push: "$$ROOT" }
      }
    }
  ]);
};

// setInterval(() => {
//   getHeatMap(9).then(data=>{console.log('heatmapData:', data)})
// }, 1000);

/**
 * Get heat map
 * @public
 */
exports.heatmap = async (req, res, next) => {
  getHeatMap(9).then((data) => {
    // let total = 0;
    // data.forEach(d=>{
    //   total+=d.count;
    // })
    // console.log('total',total)
    res.json(
      data.map((d) => {
        const decodedCoordinates = Geohash.decode(d._id);
        return {
          lat: decodedCoordinates.lat,
          lng: decodedCoordinates.lon,
          weight: d.count,
          zipcode: d.zipcode,
        };
      })
    )
  })
};

/**
 * Save new itechUser location
 * @public
 */
exports.location = async (req, res, next) => {
  if (_.isNil(req.body.lng) || _.isNil(req.body.lat)) {
    res.json({
      success: false,
      error: "latitude or longitude params not sent"
    });
  }
  try {
    // Create location entry
    const location = new Location({
      userId: req.user.get("id"),
      loc: { type: "Point", coordinates: [req.body.lng, req.body.lat] }
    });
    // Persist
    const savedLocation = await location.save();
    // Return response
    res.json({ success: true, location: savedLocation }, httpStatus.CREATED);
  } catch (error) {
    next(error);
  }
};
