/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const sinon = require('sinon');
const bcrypt = require('bcryptjs');
const { some, omitBy, isNil } = require('lodash');
const app = require('../../../index');
const User = require('../../models/user.model');
const JWT_EXPIRATION = require('../../../config/vars').jwtExpirationInterval;

/**
 * root level hooks
 */

async function format(user) {
  const formated = user;

  // delete password
  delete formated.password;

  // get users from database
  const dbUser = (await User.findOne({ email: user.email })).transform();

  // remove null and undefined properties
  return omitBy(dbUser, isNil);
}

describe('Users API', async () => {
  let adminAccessToken;
  let userAccessToken;
  let dbUsers;
  let user;
  let admin;
  const apiVersion = '';

  const password = '123456';
  const passwordHashed = await bcrypt.hash(password, 1);

  beforeEach(async () => {
    dbUsers = {
      testUser: {
        email: 'test@gmail.com',
        password: passwordHashed,
        name: 'Test',
      },
    };

    user = {
      email: 'test@gmail.com',
      password,
      name: 'Test',
    };


    await User.remove({});
    await User.insertMany([dbUsers.branStark, dbUsers.jonSnow]);
    dbUsers.branStark.password = password;
    dbUsers.jonSnow.password = password;
    userAccessToken = (await User.findAndGenerateToken(dbUsers.jonSnow)).accessToken;
  });

  describe(`POST ${apiVersion}/users`, () => {
    it('should create a new user when request is ok', () => {
      return request(app)
        .post(`${apiVersion}/users`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(admin)
        .expect(httpStatus.CREATED)
        .then((res) => {
          delete admin.password;
          expect(res.body).to.include(admin);
        });
    });
  });

  describe(`GET ${apiVersion}/users/:userId`, () => {
    it('should get user', async () => {
      const id = (await User.findOne({}))._id;
      delete dbUsers.branStark.password;

      return request(app)
        .get(`${apiVersion}/users/${id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.include(dbUsers.branStark);
        });
    });
  });
});
