const mongoose = require('mongoose');
// const Geohash = require('latlon-geohash');
const geohash = require('../../../../frontend/geohash');
// var geocoder = require('local-reverse-postalcode');

let reverse = require('reverse-geocode');

/**
 * Location Schema
 * @private
 */
const Location = new mongoose.Schema(
  {
    creationDate: { type: Date, default: Date.now },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    loc: {
      type: { type: String, enum: 'Point', default: 'Point' },
      coordinates: { type: [Number], default: [0, 0] },
    },
    geohash: { type: String },
    zipcode: { type: String },
    username: { type: String },
  },
  {
    timestamps: true,
  },
);

Location.pre('save', function(next) {
  const lng = parseFloat(this.loc.coordinates[0]);
  const lat = parseFloat(this.loc.coordinates[1]);
  console.log('lat/lng:', lat,lng)
  this.geohash = geohash.encode(lat, lng);
  this.zipcode = reverse.lookup(lat,lng, 'us').zipcode;
  console.log('this.zipcode',this.zipcode)
  next();
});

Location.index({ loc: '2dsphere' });

module.exports = mongoose.model('Location', Location);
