import './App.css'
import React, {Component} from 'react'
import Map from './map'

class App extends Component {
  render() {
    return <div className="App">
      <Map></Map>
    </div>
  }
}

export default App
